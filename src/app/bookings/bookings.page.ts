import { Component, OnDestroy, OnInit }      from '@angular/core';
import { BookingsService }                   from './bookings.service';
import { Booking }                           from './booking.model';
import { IonItemSliding, LoadingController } from '@ionic/angular';
import { Router }                            from '@angular/router';
import { Subscription }                      from 'rxjs';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit, OnDestroy {
  loadedBookings: Booking[];
  bookingsSub: Subscription;

  constructor(
    private bookingsService: BookingsService,
    private router: Router,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.bookingsSub = this.bookingsService.bookings.subscribe(bookings => {
      this.loadedBookings = bookings;
    });
  }

  onCancelBooking( bookingId: string, slidingBooking: IonItemSliding ) {
    slidingBooking.close();
    this.loadingCtrl.create({message: 'Canceling...'})
      .then(loadingEl => {
        loadingEl.present();
        this.bookingsService.cancelBooking(bookingId)
            .subscribe(() => {
              loadingEl.dismiss();
            });

      });
  }

  ngOnDestroy(): void {
    if (this.bookingsSub) {
      this.bookingsSub.unsubscribe();
    }
  }
}
