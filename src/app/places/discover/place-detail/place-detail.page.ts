import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  ActionSheetController, LoadingController, ModalController, NavController
}                                       from '@ionic/angular';
import { ActivatedRoute }               from '@angular/router';
import { Subscription }                 from 'rxjs';
import { AuthService }                  from '../../../auth/auth.service';
import { PlacesService }                from '../../places.service';
import { Place }                        from '../../place.model';
import { CreateBookingComponent }       from '../../../bookings/create-booking/create-booking.component';
import { BookingsService }              from '../../../bookings/bookings.service';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit, OnDestroy {
  place: Place;
  isBookable = false;
  placeSub: Subscription;

  constructor(
    private navCtrl: NavController,
    private placesService: PlacesService,
    private route: ActivatedRoute,
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private bookingService: BookingsService,
    private loadingCtrl: LoadingController,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if ( !paramMap.has('placeId')) {
        this.navCtrl.navigateBack('/places/tabs/discover');
        return;
      }
      this.placeSub = this.placesService.getPlace( paramMap.get('placeId')).subscribe(place => {
        this.place = place;
        this.isBookable = place.userId !== this.authService.userId;
      });
    });
  }

  onBookPlace() {
    // this.navCtrl.navigateBack(['/places/tabs/discover']);
    // this.navCtrl.pop();

    this.actionSheetCtrl
      .create({
        header: 'Choose an action',
        buttons: [
          { text: 'Select date', handler: () => { this.openBookingModal('select'); } },
          { text: 'Random date', handler: () => { this.openBookingModal('random'); }},
          { text: 'Cancel', role: 'cancel'},
        ]
      })
      .then(actionSheetEl => actionSheetEl.present() );
  }

  openBookingModal(mode: 'select' | 'random') {
    console.log('+++ [PlaceDetailPage] openBookingModal() mode', mode);
    this.modalCtrl
      .create({
        component: CreateBookingComponent,
        componentProps: {selectedPlace: this.place, selectedMode: mode}
      })
      .then(modalEl => {
        modalEl.present();
        return modalEl.onDidDismiss();
      })
      .then(resultData => {
        console.log('+++ [PlaceDetailPage] resultData of create modal', resultData.data, resultData.role);
        if (resultData.role === 'confirm') {
          this.loadingCtrl.create({message: 'Booking place...'})
            .then(loadingEl => {
              loadingEl.present();
              const data = resultData.data.bookingData;
              this.bookingService.addBooking(
                this.place.id,
                this.place.title,
                this.place.imageUrl,
                data.firstName,
                data.lastName,
                data.guestNumber,
                data.startDate,
                data.endDate
              )
                .subscribe(() => {
                  loadingEl.dismiss();
                });
            });
        }
      });
  }

  ngOnDestroy(): void {
    if (this.placeSub) {
      this.placeSub.unsubscribe();
    }
  }
}
