import { Injectable }                       from '@angular/core';
import { HttpClient }                       from '@angular/common/http';
import { BehaviorSubject }                  from 'rxjs';
import { delay, map, switchMap, take, tap } from 'rxjs/operators';
import { Place }                            from './place.model';
import { AuthService }                      from '../auth/auth.service';

interface PlaceData {
  availableFrom: string;
  availableTo: string;
  description: string;
  imageUrl: string;
  price: number;
  title: string;
  userId: string;
}

/*
 [
 new Place(
 'p1',
 'Manhattan Mansion',
 'In the heart of New York City.',
 'https://australianfintech.com.au/wp-content/uploads/2018/08/new-york-aerial.jpg',
 149.99,
 new Date ('2019-01-01'),
 new Date ('2019-12-31'),
 'abc'
 ),
 new Place(
 'p2',
 'L\'Amour Toujours',
 'A romantic place in Paris!',
 'https://cdn-images-1.medium.com/max/1600/1*t-nXIcaD3oP6CS4ydXV1xw.jpeg',
 189.99,
 new Date ('2019-01-01'),
 new Date ('2019-07-01'),
 'xyz'
 ),
 new Place(
 'p3',
 'The Foggy Palace',
 'Not your average city place!',
 'https://i1.trekearth.com/photos/138102/dsc_0681.jpg',
 99.99,
 new Date ('2019-01-01'),
 new Date ('2020-07-01'),
 'xyz'
 ),

 ]
*/

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  private _places = new BehaviorSubject<Place[]>([]);

  constructor(private authService: AuthService, private http: HttpClient) { }

  get places() {
    return this._places.asObservable();
  }

  fetchPlaces() {
    return this.http
      .get<{[key: string]: PlaceData}>('https://ionic-angular-course-c5f05.firebaseio.com/offered-places.json')
      .pipe(
        map(resData => {
          console.log('+++[PlacesService] fetched data', resData);
          const places = [];
          for (const key in resData) {
            if (resData.hasOwnProperty(key)) {
              places.push(
                new Place(
                  key,
                  resData[key].title,
                  resData[key].description,
                  resData[key].imageUrl,
                  resData[key].price,
                  new Date(resData[key].availableFrom),
                  new Date(resData[key].availableTo),
                  resData[key].userId
                )
              );
            }
          }
          return places;
        }),
        tap(places => this._places.next(places))
      );
  }

  getPlace(id: string) {
    return this.places.pipe(
      take(1),
      map(places => {
        return { ...places.find(p => p.id === id) };
      })
    );
  }

  addPlace(title: string, description: string, price: number, dateFrom: Date, dateTo: Date) {
    let generatedId: string;
    const newPlace = new Place(
      Math.random().toString(),
      title,
      description,
      'https://i1.trekearth.com/photos/138102/dsc_0681.jpg',
      price,
      dateFrom,
      dateTo,
      this.authService.userId
    );
    return this.http
      .post<{name: string}>(
        'https://ionic-angular-course-c5f05.firebaseio.com/offered-places.json',
        {
          ...newPlace,
          id: null
        }
      )
      .pipe(
        switchMap(resData => {
          console.log(resData);
          generatedId = resData.name;
          return this.places;
        }),
        take(1),
        tap(places => {
          newPlace.id = generatedId;
          this._places.next( places.concat( newPlace ));
        })
      );
  }

  updatePlace( placeId: string, title: string, description: string) {
    let updatedPlaces: Place[];
    return this.places.pipe(
      take(1), switchMap(places => {
        const updatePlaceIndex = places.findIndex(pl => pl.id === placeId);
        updatedPlaces = [...places];
        const oldPlace = updatedPlaces[updatePlaceIndex];
        updatedPlaces[updatePlaceIndex] = new Place(
          oldPlace.id,
          title,
          description,
          oldPlace.imageUrl ,
          oldPlace.price,
          oldPlace.availableFrom,
          oldPlace.availableTo,
          oldPlace.userId
        );
        return this.http.put(`https://ionic-angular-course-c5f05.firebaseio.com/offered-places/${placeId}.json`,
                             {...updatedPlaces[updatePlaceIndex], id: null});
      }), tap(() => {
        this._places.next(updatedPlaces);
      })
    );
  }
}
